const {
    getITRInfo, parseGstCertificate, gstrParse
} = require('./controller/index');

module.exports = {
    parseITR: getITRInfo,
    parseGSTCertificate: parseGstCertificate,
    parseGSTR3B: gstrParse
}