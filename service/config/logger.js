const log4js = require("log4js");

log4js.configure({
  appenders: {

    console: {
      type: "file",
      filename: `consoleLogger.log`,
      layout: {
        type: "pattern",
        pattern: "%d{yyyy-MM-dd hh:mm:ss.SSS} %p %c %m - %f{1}:%l"
      },
      compress: true,
      keepFileExt: true
    },

    pdfParser: {
      type: "file",
      filename: `pdfParser.log`,
      layout: {
        type: "pattern",
        pattern: "%d{yyyy-MM-dd hh:mm:ss.SSS} %p %c %m - %f{1}:%l"
      },
      maxLogSize: 10485760,
      compress: true,
      keepFileExt: true
    }
  },
  categories: {
    default: { appenders: ["pdfParser"], level: "debug", enableCallStack: true },
    console: { appenders: ["console"], level: "info" }
  }

});

const logger = log4js.getLogger();
const consoleLogger = log4js.getLogger("console");

module.exports = { logger, consoleLogger };
