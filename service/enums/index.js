const defaultValues = {
    ITR_3_2018_19: 'ITR_3_2018_19',
    ITR_3_2019_20: 'ITR_3_2019_20',
    ITR_3_2020_21: 'ITR_3_2020_21',
    ITR_4_2018_19: 'ITR_4_2018_19',
    ITR_4_2019_20: 'ITR_4_2019_20',
    ITR_4_2020_21: 'ITR_4_2020_21',
    ITR_5_2018_19: 'ITR_5_2018_19',
    ITR_5_2019_20: 'ITR_5_2019_20',
    ITR_5_2020_21: 'ITR_5_2020_21',
    ITR_7_2019_20: 'ITR_7_2019_20',
    ITR_FORM_TYPES: ['ITR-3', 'ITR-5', 'ITR 7','ITR 4'],
    ASSESSMENT_YEAR_PATTER: 'Assessment Year : ',
    LAST_12_MONTH: 11
}

module.exports = { defaultValues };