
const { trim } = require('lodash');

const validator = require('validator');

const validationLabels = require('./validationLabel')

const { PAN_NUMBER_REG_EXP } = require('./regexExpression')

const {
    NUMBER,
    IS_EMPTY,
    IS_NOT_EMPTY,
    PAN_NUMBER,
    NUMBER_WITH_SIGN
} = validationLabels;

/**
 * @description Function for validating the input form fields
 * @name isValid
 * @param {String} field 
 * @param {String} value 
 */
const isValid = (field, stringValue) => {

    let value = trim(stringValue);

    let validationResult = false;

    switch (field) {

        case IS_EMPTY: validationResult = validator.isEmpty(`${value}`)
            break;

        case IS_NOT_EMPTY: validationResult = !validator.isEmpty(`${value}`)
            break;

        case NUMBER: validationResult = validator.isNumeric(`${value}`, { no_symbols: true })
            break;

        case NUMBER_WITH_SIGN: validationResult = validator.isNumeric(`${value}`)
            break;

        case PAN_NUMBER: validationResult = PAN_NUMBER_REG_EXP.test(value)
            break;

        default: validationResult = false
            break;

    }


    return validationResult;

};

module.exports = { isValid }